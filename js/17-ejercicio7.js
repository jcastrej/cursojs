'use strict'

/* Mostrar tabla de multiplicar de un numero inteoducido en pantalla*/

var numero = parseInt(prompt("Introduce el numero", 0));
var res = 0;

while(isNaN(numero) || numero == 0){
    var numero = parseInt(prompt("Valor incorrecto, intenta de nuevo", 0));
}

document.write("<h1>Tabla del " + numero + "</h1> <br>");
for (let i = 1; i <= 10; i++){
    res = numero*i;
    document.write(numero + " x " + i + " = " + res + "<br>");
}