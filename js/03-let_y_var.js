'use strict'
//Let y var

// Prueba con var

var numero = 40; 
console.log(numero);  // valor 40 

if(true){
	var numero = 50;
	console.log(numero);  //valor 50
}

console.log(numero);

// Prueba con Let

var texto = "Hola mundo con JS";
console.log(texto);

if (true) {
	let texto = "Hola mundo con Laravel";
	console.log(texto);
}

console.log(texto);
