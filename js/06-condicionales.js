'use strict'

var edad = 34;
var nombre = 'Jose Castrejon'

var year = 2028;

//Uso de IF

if (edad >= 18) {
	
	console.log("El usuario " + nombre + " es mayor de edad");
	
	if (edad <= 33) {
		console.log("Ya no eres millennial");
	}
}else if(edad >= 70){
	console.log("Eres anciano");
}

/*
OPERADORES LOGICOS AND OR Y NOT
&& || !
*/

if (year != 2016) {
	console.log("El año no es 2016, el año correcto es " + year);
}

if (year >= 2000 && year <= 2020) {
	console.log("Estamos en la era actual");
}else{
	console.log("Estamos en la era posmoderna");
}

if(year == 2008 || (year >= 2018 && year == 2028)){
	console.log("El año acaba en 8");
}else{
	console.log("Año no registrado");
}