'use strict'

//Operadores

var numero1 = 1
var numero2 = 7
var operacion = numero2 + numero1;

//alert("El resultado de la operacion es "+ operacion); 

// Tipos de datos

var entero = 44;
var cadena = "Hola 'que' mundo";
var verdadero_falso = true;
var numero_falso = "33.4";

console.log(typeof entero);
console.log(typeof cadena);
console.log(typeof verdadero_falso);
console.log(typeof numero_falso);


