'use strict'

//funciones anonimas
/* var pelicula = function(nombre) {
    return "La pelicula es " + nombre;
} */
// El callback es una funcion dentro de otra funcion

var sumame = function(num1, num2, sumaYmuestra, sumaPorDos) {
    var sumar = num1 + num2;
    sumaYmuestra(sumar);
    sumaPorDos(sumar);

    return sumar;
}


sumame(5,7, (dato) => {
    console.log("La suma es: "  + dato);
}, (dato) => {
    console.log("La suma por dos es: "  + (dato*2));
});


