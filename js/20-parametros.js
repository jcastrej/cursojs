'use strict'

// vamos a mandar parametros a la funcion de la calculadora

//var numero1 = parseFloat(prompt("Primer numero", 0));
//var numero2 = parseFloat(prompt("SEgundo numero", 0));
function porConsola(numero1, numero2) {
    console.log("Suma " + (numero1 + numero2));
    console.log("Resta " + (numero1 - numero2));
    console.log("Multiplicacion " + (numero1 * numero2));
    console.log("Division " + (numero1 / numero2));
    console.log("************************************");
}

function porPantalla(numero1, numero2) {
    document.write("***********************************"+"<br\>")
    document.write("Suma " + (numero1 + numero2) + "<br\>");
    document.write("Resta " + (numero1 - numero2) + "<br\>");
    document.write("Multiplicacion " + (numero1 * numero2) + "<br\>");
    document.write("Division " + (numero1 / numero2) + "<br\>");
    document.write("************************************");
}

function calculadora(numero1, numero2, mostrar = false) {
    if (mostrar == false) {
        porConsola(numero1,numero2);
    } else {
        porPantalla(numero1,numero2);
    }
}

//calculadora(10,35);
//calculadora(numero1, numero2);

for (let i = 1; i <= 10; i++) {
    console.log(i);
    calculadora(i,8, true);
}



